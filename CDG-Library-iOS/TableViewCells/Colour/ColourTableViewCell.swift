//
//  ColourTableViewCell.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

class ColourTableViewCell: UITableViewCell {
    // MARK: - Views & Controls
    private let colorView = UIView()
    private let nameLabel = UILabel()
    
    // MARK: - Properties
    static let reuseIdentifier = String(describing: ColourTableViewCell.self)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addHierarchy()
        configureHierarchy()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutHierarchy()
    }
}

extension ColourTableViewCell {
    private func addHierarchy() {
        addSubview(colorView)
        addSubview(nameLabel)
    }
    
    private func configureHierarchy() {
        configureColorView()
    }
    
    private func layoutHierarchy() {
        layoutColorView()
        layoutNameLabel()
    }
    
    private func configureColorView() {
        colorView.layer.cornerRadius = 20
        colorView.layer.borderWidth = 1.0
        colorView.layer.borderColor = Colours.Neutral.black?.cgColor
    }
    
    private func layoutColorView() {
        let inset: CGFloat = 16
        colorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            colorView.topAnchor.constraint(equalTo: topAnchor, constant: inset / 2),
            colorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: inset),
            colorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset / 2),
            colorView.widthAnchor.constraint(equalToConstant: 40),
            colorView.heightAnchor.constraint(equalTo: colorView.widthAnchor)
        ])
    }
    
    private func layoutNameLabel() {
        let inset: CGFloat = 16
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: colorView.trailingAnchor, constant: inset),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -inset),
            nameLabel.centerYAnchor.constraint(equalTo: colorView.centerYAnchor)
        ])
    }
}

extension ColourTableViewCell {
    func configure(viewModel: ColourViewModel) {
        colorView.backgroundColor = viewModel.color
        nameLabel.text = viewModel.name
    }
}
