//
//  ColourViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

struct ColourViewModel {
    let color: UIColor?
    let name: String
}

extension ColourViewModel {
    // MARK: - Primary
    static let azure = ColourViewModel(color: Colours.Primary.azure, name: "Azure")
    static let orange = ColourViewModel(color: Colours.Primary.orange, name: "Orange")
    static let yellow = ColourViewModel(color: Colours.Primary.yellow, name: "Yellow")
    static let primary: [ColourViewModel] = [.azure, .orange, .yellow]
    
    // MARK: - Secondary
    static let darkIndigo = ColourViewModel(color: Colours.Secondary.darkIndigo, name: "Dark Indigo")
    static let peacockblue = ColourViewModel(color: Colours.Secondary.peacockBlue, name: "Peacock Blue")
    static let sky = ColourViewModel(color: Colours.Secondary.sky, name: "Sky")
    static let green = ColourViewModel(color: Colours.Secondary.green, name: "Green")
    static let warmGrey = ColourViewModel(color: Colours.Secondary.warmGrey, name: "Warm Grey")
    static let secondary: [ColourViewModel] = [.darkIndigo, .peacockblue, .sky, .green, .warmGrey]
    
    // MARK: - Neutral
    static let black = ColourViewModel(color: Colours.Neutral.black, name: "Black")
    static let paleGrey = ColourViewModel(color: Colours.Neutral.paleGrey, name: "Pale Grey")
    static let white = ColourViewModel(color: Colours.Neutral.white, name: "White")
    static let neutral: [ColourViewModel] = [.black, .paleGrey, .white]
}
