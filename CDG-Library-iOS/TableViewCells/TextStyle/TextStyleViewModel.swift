//
//  TextStyleViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import  UIKit

struct TextStyleViewModel {
    let text: String
    let font: UIFont?
}

extension TextStyleViewModel {
    // MARK: - Headings
    static let h1 = TextStyleViewModel(
        text: "H1 lorem ipsum dolar sit amet",
        font: TextStyles.h1
    )
    
    static let h2 = TextStyleViewModel(
        text: "H2 lorem ipsum dolor sit amet, consectetur adipiscing elit",
        font: TextStyles.h2
    )
    
    static let h3 = TextStyleViewModel(
        text: "H3 lorem ipsum dolor sit amet, consectetur adipiscing elit",
        font: TextStyles.h3
    )
    
    static let h4 = TextStyleViewModel(
        text: "H4 lorem ipsum dolor sit amet, consectetur adipiscing elit",
        font: TextStyles.h4
    )
    
    static let headings: [TextStyleViewModel] = [.h1, .h2, .h3, .h4]
    
    // MARK: - Paragraphs
    static let p1 = TextStyleViewModel(
        text: "P1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec laoreet pellentesque felis quis egestas. Phasellus molestie rhoncus sem, et mollis nunc vestibulum non. In vel mauris id nisl congue pellentesque. \n\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam aliquet, lorem quis dictum viverra, lectus turpis tempus eros, sed porttitor felis lorem ac odio.",
        font: TextStyles.p1
    )
    
    static let p2 = TextStyleViewModel(
        text: "P2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec laoreet pellentesque felis quis egestas. Phasellus molestie rhoncus sem, et mollis nunc vestibulum non. In vel mauris id nisl congue pellentesque. \n\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam aliquet, lorem quis dictum viverra, lectus turpis tempus eros, sed porttitor felis lorem ac odio.",
        font: TextStyles.p2
    )
    
    static let paragraphs: [TextStyleViewModel] = [.p1, .p2]
    
    // MARK: - Labels
    static let l1 = TextStyleViewModel(
        text: "L1 label ipsum dolor sit amet",
        font: TextStyles.l1
    )
    
    static let l2 = TextStyleViewModel(
        text: "L2 label ipsum dolor sit amet",
        font: TextStyles.l2
    )
    
    static let l3 = TextStyleViewModel(
        text: "L3 label ipsum dolor sit amet",
        font: TextStyles.l3
    )
    
    static let l4 = TextStyleViewModel(
        text: "L4 small label ipsum dolor sit amet",
        font: TextStyles.l4
    )
    
    static let labels: [TextStyleViewModel] = [.l1, .l2, .l3, .l4]
    
    // MARK: - CTA (Call-to-actions)
    static let c1 = TextStyleViewModel(
        text: "C1 label ipsum dolor sit amet",
        font: TextStyles.c1
    )
    
    static let c2 = TextStyleViewModel(
        text: "C2 label ipsum dolor sit amet",
        font: TextStyles.c2
    )
    
    static let c3 = TextStyleViewModel(
        text: "C3 label ipsum dolor sit amet",
        font: TextStyles.c3
    )
    
    static let ctas: [TextStyleViewModel] = [.c1, .c2, .c3]
}
