//
//  TextStyleTableViewCell.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

class TextStyleTableViewCell: UITableViewCell {
    // MARK: - Views & Controls
    private let label = UILabel()
    
    // MARK: - Properties
    static let reuseIdentifier = String(describing: TextStyleTableViewCell.self)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addHierarchy()
        configureHierarchy()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutHierarchy()
    }
}

extension TextStyleTableViewCell {
    private func addHierarchy() {
        addSubview(label)
    }
    
    private func configureHierarchy() {
        configureLabel()
    }
    
    private func layoutHierarchy() {
        layoutLabel()
    }
    
    private func configureLabel() {
        label.numberOfLines = 0
    }
    
    private func layoutLabel() {
        let inset: CGFloat = 16
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor, constant: inset / 2),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: inset),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset / 2),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -inset)
        ])
    }
}

extension TextStyleTableViewCell {
    func configure(viewModel: TextStyleViewModel) {
        label.text = viewModel.text
        label.font = viewModel.font
    }
}
