//
//  Colours.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

enum Colours {
    // MARK: - Primary
    enum Primary {
        static let azure = UIColor(hex: "#008bfbff")
        static let orange = UIColor(hex: "#ff9900ff")
        static let yellow = UIColor(hex: "#f7d718ff")
    }
    
    // MARK: - Secondary
    enum Secondary {
        static let darkIndigo = UIColor(hex: "#080c4eff")
        static let peacockBlue = UIColor(hex: "#005fabff")
        static let sky = UIColor(hex: "#7bd7ffff")
        static let green = UIColor(hex: "#7ed321ff")
        static let warmGrey = UIColor(hex: "#9b9b9bff")
    }
    
    // MARK: - Neutral
    enum Neutral {
        static let black = UIColor(hex: "#161616ff")
        static let paleGrey = UIColor(hex: "#f5f7f8ff")
        static let white = UIColor(hex: "#ffffffff")
    }
}
