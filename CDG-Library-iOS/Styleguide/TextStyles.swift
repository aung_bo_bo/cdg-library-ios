//
//  TextStyles.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

enum TextStyles {
    static let berlowMedium = "Barlow-Medium"
    static let berlowBold = "Barlow-Bold"
    
    // MARK: - Headings
    static let h1 = UIFont(name: berlowBold, size: 28)
    static let h2 = UIFont(name: berlowBold, size: 24)
    static let h3 = UIFont(name: berlowBold, size: 20)
    static let h4 = UIFont(name: berlowBold, size: 16)
    
    // MARK: - Paragraphs
    static let p1 = UIFont(name: berlowMedium, size: 16)
    static let p2 = UIFont(name: berlowMedium, size: 13)
    
    // MARK: - Labels
    static let l1 = UIFont(name: berlowMedium, size: 14)
    static let l2 = UIFont(name: berlowMedium, size: 13)
    static let l3 = UIFont(name: berlowMedium, size: 12)
    static let l4 = UIFont(name: berlowMedium, size: 10)
    
    // MARK: - Call to actions
    static let c1 = UIFont(name: berlowBold, size: 18)
    static let c2 = UIFont(name: berlowBold, size: 16)
    static let c3 = UIFont(name: berlowBold, size: 12)
}
