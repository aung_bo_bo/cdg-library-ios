//
//  ColoursSection.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

enum ColoursSection: String, CaseIterable {
    case primary = "Primary"
    case secondary = "Secondary"
    case neutral = "Neutral"
    
    init?(section: Int) {
        switch section {
        case 0:
            self = .primary
        case 1:
            self = .secondary
        case 2:
            self = .neutral
        default:
            return nil
        }
    }
}
