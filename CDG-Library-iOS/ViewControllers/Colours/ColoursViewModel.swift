//
//  ColoursViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

struct ColoursViewModel {
    let title = "Colours"
    
    var sectionsCount: Int { ColoursSection.allCases.count }
    
    func getItemsCount(for section: ColoursSection) -> Int {
        switch section {
        case .primary:
            return ColourViewModel.primary.count
        case .secondary:
            return ColourViewModel.secondary.count
        case .neutral:
            return ColourViewModel.neutral.count
        }
    }
    
    func getItem(for indexPath: IndexPath) -> ColourViewModel? {
        guard let section = ColoursSection(section: indexPath.section) else { return nil }
        switch section {
        case .primary:
            return ColourViewModel.primary[indexPath.row]
        case .secondary:
            return ColourViewModel.secondary[indexPath.row]
        case .neutral:
            return ColourViewModel.neutral[indexPath.row]
        }
    }
}
