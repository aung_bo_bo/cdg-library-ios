//
//  TextStylesSection.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

enum TextStylesSection: String, CaseIterable {
    case headings = "Headings"
    case paragraphs = "Paragraphs"
    case labels = "Labels"
    case ctas = "CTA (Call-to-actions)"
    
    init?(section: Int) {
        switch section {
        case 0:
            self = .headings
        case 1:
            self = .paragraphs
        case 2:
            self = .labels
        case 3:
            self = .ctas
        default:
            return nil
        }
    }
}
