//
//  TextStylesViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

struct TextStylesViewModel {
    let title = "Text"
    
    var sectionsCount: Int { TextStylesSection.allCases.count }
    
    func getItemsCount(for section: TextStylesSection) -> Int {
        switch section {
        case .headings:
            return TextStyleViewModel.headings.count
        case .paragraphs:
            return TextStyleViewModel.paragraphs.count
        case .labels:
            return TextStyleViewModel.labels.count
        case .ctas:
            return TextStyleViewModel.ctas.count
        }
    }
    
    func getItem(for indexPath: IndexPath) -> TextStyleViewModel? {
        guard let section = TextStylesSection(section: indexPath.section) else { return nil }
        switch section {
        case .headings:
            return TextStyleViewModel.headings[indexPath.row]
        case .paragraphs:
            return TextStyleViewModel.paragraphs[indexPath.row]
        case .labels:
            return TextStyleViewModel.labels[indexPath.row]
        case .ctas:
            return TextStyleViewModel.ctas[indexPath.row]
        }
    }
}


