//
//  TextStylesViewController.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

class TextStylesViewController: UIViewController {
    // MARK: - Views & Controls
    private let tableView = UITableView()
    
    // MARK: - Properties
    private let viewModel = TextStylesViewModel()
}

// MARK: - View Lifecycle
extension TextStylesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        addHierarchy()
        configureHierarchy()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutHierarchy()
    }
}

extension TextStylesViewController {
    private func addHierarchy() {
        view.addSubview(tableView)
    }
    
    private func configureHierarchy() {
        configureNavigationItem()
        configureTableView()
    }
    
    private func layoutHierarchy() {
        layoutTableView()
    }
    
    private func configureNavigationItem() {
        navigationItem.title = viewModel.title
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(
            TextStyleTableViewCell.self,
            forCellReuseIdentifier: TextStyleTableViewCell.reuseIdentifier
        )
    }
    
    private func layoutTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}

// MARK: - UITableViewDataSource
extension TextStylesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionsCount
    }
    
    func tableView(
        _ tableView: UITableView,
        titleForHeaderInSection section: Int
    ) -> String? {
        guard let section = TextStylesSection(section: section) else { return nil }
        return section.rawValue
    }
    
    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        guard let section = TextStylesSection(section: section) else { return 0 }
        return viewModel.getItemsCount(for: section)
    }
    
    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: TextStyleTableViewCell.reuseIdentifier,
            for: indexPath
        ) as! TextStyleTableViewCell
        guard let viewModel = viewModel.getItem(for: indexPath) else { return cell }
        cell.configure(viewModel: viewModel)
        cell.layoutSubviews()
        return cell
    }
}

// MARK: - UITableViewDelegate
extension TextStylesViewController: UITableViewDelegate {
    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
