//
//  StyleguideViewController.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

class StyleguideViewController: UIViewController {
    // MARK: - Views & Controls
    private let tableView = UITableView()
    
    // MARK: - Properties
    private let viewModel = StyleguideViewModel()
}

// MARK: - View Lifecycle
extension StyleguideViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        addHierarchy()
        configureHierarchy()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutHierarchy()
    }
}

extension StyleguideViewController {
    private func addHierarchy() {
        view.addSubview(tableView)
    }
    
    private func configureHierarchy() {
        configureNavigationItem()
        configureTableView()
    }
    
    private func layoutHierarchy() {
        layoutTableView()
    }
    
    private func configureNavigationItem() {
        navigationItem.title = viewModel.title
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func layoutTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}

// MARK: - UITableViewDataSource
extension StyleguideViewController: UITableViewDataSource {
    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return viewModel.itemsCount
    }
    
    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = viewModel.items[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension StyleguideViewController: UITableViewDelegate {
    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch viewModel.items[indexPath.row] {
        case viewModel.items[0]:
            let textStylesViewController = TextStylesViewController()
            navigationController?.pushViewController(textStylesViewController, animated: true)
        case viewModel.items[1]:
            let coloursViewController = ColoursViewController()
            navigationController?.pushViewController(coloursViewController, animated: true)
        default:
            break
        }
    }
}
