//
//  StyleguideViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

struct StyleguideViewModel {
    let title = "Styleguide"
    
    let items = ["Text", "Colours"]
    var itemsCount: Int { items.count }
}
