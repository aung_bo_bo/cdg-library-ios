//
//  ComponentsViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

struct ComponentsViewModel {
    let title = "Components"
    
    let items = [
        "Form",
        "Controls",
        "Cards",
        "CTA",
        "Data",
        "Header",
        "Icons",
        "Prompts",
        "Template",
        "Graphics",
        "Notifications",
        "Labels"
    ]
    var itemsCount: Int { items.count }
}
