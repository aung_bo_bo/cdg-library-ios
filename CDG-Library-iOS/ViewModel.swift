//
//  ViewModel.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import Foundation

struct ViewModel {
    let title = "CDG-Library"
    
    let items = ["Styleguide", "Components"]
    var itemsCount: Int { items.count }
}
