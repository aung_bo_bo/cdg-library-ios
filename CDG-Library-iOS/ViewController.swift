//
//  ViewController.swift
//  CDG-Library-iOS
//
//  Created by Aung Bo Bo on 11/08/2021.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - Views & Controls
    private let tableView = UITableView()
    
    // MARK: - Properties
    private let viewModel = ViewModel()
}

// MARK: - View Lifecycle
extension ViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        addHierarchy()
        configureHierarchy()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutHierarchy()
    }
}

extension ViewController {
    private func addHierarchy() {
        view.addSubview(tableView)
    }
    
    private func configureHierarchy() {
        configureNavigationItem()
        configureTableView()
    }
    
    private func layoutHierarchy() {
        layoutTableView()
    }
    
    private func configureNavigationItem() {
        navigationItem.title = viewModel.title
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func layoutTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return viewModel.itemsCount
    }
    
    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = viewModel.items[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ViewController: UITableViewDelegate {
    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch viewModel.items[indexPath.row] {
        case viewModel.items[0]:
            let styleguideViewController = StyleguideViewController()
            navigationController?.pushViewController(styleguideViewController, animated: true)
        case viewModel.items[1]:
            let componentsViewController = ComponentsViewController()
            navigationController?.pushViewController(componentsViewController, animated: true)
        default:
            break
        }
    }
}
